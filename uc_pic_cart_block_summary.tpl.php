<?php

/**
 * @file
 * UC Pictured cart block content.
 * 
 * Available variables:
 *   - $count: How many items are in cart.
 *   - $total: How much money we want to spend :)
 */

/* @var $count integer */
/* @var $total float */

$item_text = format_plural($count, '<span class="num-items">@count</span> Item', '<span class="num-items">@count</span> Items');
$attr = array('attributes' => array('rel' => 'nofollow', 'class' => array('btn', 'btn-success'/*, 'btn-xs'*/)), 'html' => true);
$checkout_enabled = variable_get('uc_checkout_enabled', FALSE);

?>
<p></p>
<div class="row">
  <div class="col-xs-6"><?= $item_text ?></div>
  <div class="col-xs-6 text-right"><strong><?= uc_currency_format($total) ?></strong></div>
</div>
<p></p>
<p class="text-right"><?= l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> '. t('View cart'), 'cart', $attr) ?>
<?php if ($checkout_enabled) : ?>
  <?php $attr = array('attributes' => array('rel' => 'nofollow', 'class' => array('btn', 'btn-primary'/*, 'btn-xs'*/)), 'html' => true); ?>
  <?= l('<span class="glyphicon glyphicon-gift" aria-hidden="true"></span> '. t('Checkout'), 'cart/checkout', $attr) ?>
<?php endif; ?>
</p>

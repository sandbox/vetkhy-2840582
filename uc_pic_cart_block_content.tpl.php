<?php

/**
 * @file
 * UC Pictured cart block content.
 * 
 * Available variables:
 *   - $use_scroll: boolean
 *   - $orientation: 0 is vertical, other is horisontal.
 *   - $items: cart display items @see uc_pic_cart_block_content
 *   - $path_module: Path to uc_pic_cart_block module by default.
 */

/* @var $use_scroll boolean */
/* @var $orientation integer */
/* @var $items array */
/* @var $path_module string */

$rownum = 0;
?>

<?php if ($use_scroll) : ?>
<?= theme('uc_pic_cart_block_scroll_btn', array(
  'last' => FALSE,
  'orientation' => $orientation,
)); ?>
<?php endif; ?>

<div id="uc_pic_cart_block_content" class="<?= $orientation ? 'horizontal clearfix' : 'vertical' ?>">
  <?php foreach ($items as $item) : ?>
    <?= theme('uc_pic_cart_block_item', array(
      'item' =>  $item,
      'rownum' => ++$rownum,
      'orientation' => $orientation,
    )); ?>
  <?php endforeach; ?>
</div><!-- cart content -->

<?php if ($use_scroll) : ?>
<?= theme('uc_pic_cart_block_scroll_btn', array(
  'last' => TRUE,
  'orientation' => $orientation,
)); ?>
<?php endif; ?>
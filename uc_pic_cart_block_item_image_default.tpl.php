<?php

/**
 * @file
 * UC Pictured cart product default image.
 * 
 * Available variables:
 *   - $path_module: Path to uc_pic_cart_block module by default.
 *   - $preset: image preset.
 *   - $product: product node.
 */

/* @var $path_module string */
/* @var $preset string */
/* @var $product stdClass */

$image_dimentions = getimagesize($path_module . '/img/no_image.gif');
$dim = array('width' => $image_dimentions[0], 'height' => $image_dimentions[1]);
image_style_transform_dimensions($preset, $dim);
$attributes = array('class' => 'img img-thumbnail uc_pic_cart_block_no_image');
if (!empty($dim['width'])) {
    $attributes['width'] = $dim['width'];
}
if (!empty($dim['height'])) {
    $attributes['height'] = $dim['height'];
}
$img = theme('image', array(
  'path' => $path_module .'/img/no_image.gif',
  'alt' => t('No pic'),
  'title' => check_plain($product->title) .' ('. t('no picture available') .')',
  'attributes' => $attributes
));

if (node_access('view', $product)) {
  echo l($img, 'node/'. $product->nid, array('html' => TRUE));
}
else {
  echo $img;
}

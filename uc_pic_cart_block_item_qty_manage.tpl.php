<?php

/**
 * @file
 * UC Pictured cart item qty management block.
 * 
 * Available variables:
 *   - $item: cart display item:
 *     - title: name of product (link to node in most cases),
 *     - description: description of product, e.g. attributes, parts of product kit, etc.
 *     - qty: quantity of product,
 *     - price: price of item,
 *     - nid: node ID of product,
 *     - ciid: cart item ID,
 *     - data: serialized data of product,
 *     - img: full themed image of product,
 *     - module: module of product ('uc_product', 'uc_product_kit', etc.).
 *   - $path_module: Path to uc_pic_cart_block module by default.
 *   - $restrict_feature: is product has restrict_qty feature.
 *   - $orientation: 0 is vertical, another is horisontal.
 *   - $removebtn: html-string for remove button.
 */

/* @var $item array */
/* @var $path_module string */
/* @var $restrict_feature boolean */
/* @var $orientation integer */
/* @var $removebtn string */

?>

<?php if ($restrict_feature) : ?>
    <span class="uc_pic_cart_block_spanqty"><?= $item['qty'] ?></span>
    <?php if ($orientation) : ?>
    <?= $removebtn ?>
    <?php endif; ?>
<?php else : ?>
    <input
        id="touch-<?= $item['ciid'] ?>"
        type="text"
        value="<?= $item['qty'] ?>"
        readonly="readonly"
        class="uc_pic_cart_block_touchspin input-sm"
        data-nid="<?= $item['nid'] ?>"
        data-ciid="<?= $item['ciid'] ?>"
        >
<?php endif; ?>
<?php

/**
 * @file
 * UC Pictured cart block content.
 * 
 * Available variables:
 *   - $title: A title text.
 *   - $icon_class: 'empty' or 'full' will show appropriate icon, FALSE - won't.
 *   - $path_module: Path to uc_pic_cart_block module by default.
 */

/* @var $title string */
/* @var $icon_class string */
/* @var $path_module string */

?>

<?= l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>', '/cart', array('html' => TRUE, 'attributes' => array('rel' => 'nofollow', 'class' => 'small'))) ?>

<span class="uc_pic_cart_block_block_title"><?= $title ?></span>
<?php

/**
 * @file
 * UC Pictured cart item.
 * 
 * Available variables:
 *   - $item: Array which represents current cart item:
 *     - title: name of product (link to node in most cases),
 *     - description: description of product, e.g. attributes, parts of product kit, etc.
 *     - qty: quantity of product,
 *     - price: price of item,
 *     - nid: node ID of product,
 *     - ciid: cart item ID,
 *     - data: serialized data of product,
 *     - img: full themed image of product,
 *     - module: module of product ('uc_product', 'uc_product_kit', etc.).
 *   - $rownum: Current row number (we are using odd and even styles).
 *   - $orientation: 0 is vertical, other is horisontal.
 *   - $path_module: Path to uc_pic_cart_block module by default.
 */

/* @var $item array */
/* @var $rownum integer */
/* @var $orientation integer */
/* @var $path_module string */

$h_cols = 12;
$scroll_count = variable_get('uc_pic_cart_block_scroll_count', 0);
if ($orientation && $scroll_count) {
  switch ($scroll_count) {
    case 2 :
    case 3 :
    case 4 :
    case 6 :
      $h_cols = 12 / $scroll_count;
  }
}

$itemdata = unserialize($item['data']);
$restrict_feature = (isset($itemdata['restrict_qty']['qty']) && ($itemdata['restrict_qty']['qty'] == 1));

$dest_url = drupal_get_destination();
$img_r = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
$removebtn = l($img_r, 'uccartpics/remove/nojs/'. $item['nid'] .'/'. $item['ciid'], array(
  'html' => TRUE,
  'attributes' => array('class' => 'btn btn-danger btn-xs use-ajax'),
  'query' => $dest_url
));

?>

<div class="<?= $orientation ? 'uc_pic_cart_block_item_hor text-center col-xs-' . $h_cols : 'panel panel-default item' ?>">
  <?php if ($orientation) : ?>
    <div class="item">
      <div><?= $item['img'] ?></div>
      <div class="qty">
        <?= theme('uc_pic_cart_block_item_qty_manage', array(
          'item' => $item,
          'restrict_feature' => $restrict_feature,
          'removebtn' => $removebtn,
          'orientation' => $orientation,
        )); ?>
      </div>
      <p><strong><?= $item['price'] ?></strong></p>
    </div>
  <?php else : ?>
    <div class="panel-body">
      <div class="row">
        <div class="col-xs-2 col-sm-4 col-md-3"><?= $item['img'] ?></div>
        <div class="col-xs-10 col-sm-8 col-md-9" style="padding-left: 0">
          <div class="clearfix">
            <div class="pull-right"><?= $removebtn ?></div>
            <?= $item['title'] ?>
          </div>

          <?php $use_description = (variable_get('uc_pic_cart_block_show_descriptions', FALSE) && !empty($item['description'])); ?>
          <?php if ($use_description) : ?>
          <div><?= $item['description'] ?></div>
          <?php endif; ?>

          <div class="row">
              <div class="col-xs-7 text-center qty">
                <?= theme('uc_pic_cart_block_item_qty_manage', array(
                  'item' => $item,
                  'restrict_feature' => $restrict_feature,
                  'removebtn' => $removebtn,
                  'orientation' => $orientation,
                )); ?>
              </div>
              <div class="col-xs-5 text-right"><strong><?= $item['price'] ?></strong></div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>
/* 
 * @file
 * Touchspin in uc_pictured_cart_block.
 */

(function($) {
  
  Drupal.behaviors.cartBlockTouchspin = {
    attach: function(context, settings) {
      if ($('#uc-pic-cart-block-wrapper', context).hasClass('cartBlockTouchspin-processed')) {
        return;
      };
      // jQuery.once not working as expected???
      $('#uc-pic-cart-block-wrapper', context).addClass('cartBlockTouchspin-processed');
      $('.uc_pic_cart_block_touchspin', context).removeAttr('readonly').TouchSpin({
        max: 1000000000 // almost unlimited
      }).on('change', function(event) {
        var elem = $(this);
        var nid = elem.data('nid');
        var ciid = elem.data('ciid');
        var update = elem.val();

        // building current URL - kinda hack :)
        Drupal.ajax[this.id].options.url = Drupal.settings.basePath + 'uccartpics/update/ajax/' + update + '/' + nid + '/' + ciid;
        Drupal.ajax[this.id].eventResponse(this, event);
      }).each(function() {
        var update_settings = {
          url: Drupal.settings.basePath + 'uccartpics/update/ajax/', // dummy url
          event: 'dummy.manual',
          keypress: false,
          prevent: false
        };
        Drupal.ajax[this.id] = new Drupal.ajax(this.id, $(this), update_settings);
      });
    }
  };
  
  
  
  /*$(document).ready(function () {
    cartBlockTouchspinInit();
  });*/
  
  /*if (Drupal.ajax) {
    Drupal.ajax.prototype.commands.ucPicCartAfterUpdate = function(ajax, response, status) {
      cartBlockTouchspinInit();
      if (Drupal.settings.ucPicCartBlockUseScroll) {
        initUcPicCartScroll();
      }
    }
  }*/
    
})(jQuery)
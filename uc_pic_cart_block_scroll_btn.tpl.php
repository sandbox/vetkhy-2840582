<?php

/**
 * @file
 * UC Pictured cart scroll button.
 * 
 * Available variables:
 *   - $last: Boolean. If false, we have the first part of scrolling (e.g. top),
 *            otherwise - last (e.g. bottom).
 *   - $orientation: 0 is vertical, other is horisontal.
 *   - $path_module: Path to uc_pic_cart_block module by default.
 */

/* @var $last boolean */
/* @var $orientation integer */
/* @var $path_module string */

$glyph = '';
if (!$orientation) {
  $glyph = $last ? 'menu-down' : 'menu-up';
}

?>

<?php if ($last) : ?>
</div><!-- scroll area -->
<?php endif; ?>

<?php if ($orientation) : ?>
  <?php if (!$last) : ?>
    <div class="text-right">
      <button id="uc_pic_cart_block_scroll_left" class="btn btn-default btn-xs">
        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
      </button>
      <button id="uc_pic_cart_block_scroll_right" class="btn btn-default btn-xs">
        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
      </button>
    </div>
  <?php endif; ?>
<?php else : ?>
  <div>
    <button id="uc_pic_cart_block_scroll_<?= $last ? 'down' : 'up' ?>" class="btn btn-default btn-xs btn-block">
      <span class="glyphicon glyphicon-<?= $glyph ?>" aria-hidden="true"></span>
    </button>
  </div>
<?php endif; ?>

<?php if (!$last) : ?>
<div id="uc_pic_cart_block_scroll_area"<?= $orientation ? ' class="row"' : '' ?>>
<?php endif; ?>
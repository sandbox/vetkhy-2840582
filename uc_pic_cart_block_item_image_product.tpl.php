<?php

/**
 * @file
 * UC Pictured cart product image.
 * 
 * Available variables:
 *   - $image_field: product image data
 *   - $preset: image preset.
 *   - $nid: node ID to create link.
 */

/* @var $image_field array */
/* @var $preset string */
/* @var $nid integer */

$img = theme('image_style', array(
  'style_name' => $preset,
  'title' => $image_field['title'],
  'path' => $image_field['uri'],
  'width' => $image_field['width'],
  'height' => $image_field['height'],
  'alt' => $image_field['alt'],
  'attributes' => array(
    'class' => 'img img-thumbnail',
  ),
));
?>

<?php if ($nid) : ?>
<?= l($img, 'node/'. $nid, array('html' => TRUE)) ?>
<?php else : ?>
<?= $img ?>
<?php endif; ?>
